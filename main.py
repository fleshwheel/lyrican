"""`main` is the top level module for your Flask application."""
#import flask
from flask import Flask, render_template, request
app = Flask(__name__)
from search import * #used for searching artists
from difflib import get_close_matches #used for correcting searches

def returnError(artist,word):
    global db
    if artist in db:
        #if the artist is in the database
        if not (word in db[artist]):
            #if the word selected is NOT in the database
            return "Word not in corpus for artist."
    else:
        matches = get_close_matches(artist,list(db),1)
        if matches == []:
            return "Artist not in database."
        else:
            return "Artist not in database. Did you mean %s?" % (get_close_matches(artist,list(db),1)[0])

def parse(stdin):
    """Parse commands coming into Lyrican"""
    global db
    try:
        command = stdin.split()[0]
        args = stdin.split()[1:]
        if command == "top_words":
            print topN(*args)
            print args
            return "<br>".join(topN(*args))
        if command == "times_said":
            try:
                return db[stdin.split()[1]][stdin.split()[2]]
            except KeyError:
                traceback.print_exc()
                return returnError(stdin.split()[1],stdin.split()[2])
        if command == "avg_number":
            try:
                return avgNumber(*args)
            except KeyError:
                return "Word not in database."
        if command == "diff":
            try:
                return diff(*args)
            except KeyError:
                traceback.print_exc()
                return "Artist(s) not in database."
        if command == "sim":
            try:
                return  "<br>".join(sim(*args))
            except KeyError:
                return "Artist(s) not in database."
        if command == "list_artists":
            return "<br>".join(list(db))
        else:
            return "Invalid command."
    except TypeError:
        return "Wrong number of arguments."
            
@app.route('/')
def my_form():
    """Return the start page."""
    return render_template("index.html")

@app.route('/',methods=['POST'])
def my_form_post():
    """Process input."""
    global db
    return render_template("index.html",out=str(parse(request.form['search'])))

@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return 'Sorry, there isn\'t anything at this URL.', 404

@app.errorhandler(500)
def application_error(e):
    """Return a custom 500 error."""
    return 'Sorry, unexpected error: {}'.format(e), 500